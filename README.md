# Lyo Store sample standalone project

A sample app that shows how to put IMDB movie information into the triplestore.

## Getting started

    mvn -q clean package exec:java

> **NB!** Current sample app requires access to KTH Maven repository.
